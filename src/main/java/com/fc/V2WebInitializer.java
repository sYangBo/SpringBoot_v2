package com.fc;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author fuce
 * @ClassName: SpringbootWebInitializer
 * @Description: web容器中进行部署
 * @date 2018年8月18日
 */
public class V2WebInitializer extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(V2Application.class);
    }
}
